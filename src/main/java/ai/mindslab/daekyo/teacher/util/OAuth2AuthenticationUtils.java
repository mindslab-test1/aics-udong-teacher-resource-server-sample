package ai.mindslab.daekyo.teacher.util;

import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;

import java.util.Map;

public class OAuth2AuthenticationUtils {

    public static String getTeacherName(OAuth2Authentication oAuth2Authentication) {
        return oAuth2Authentication.getPrincipal().toString();
    }

    public static String getTeacherCompanyNo(OAuth2Authentication oAuth2Authentication) {
        return getDetailsWithKey(oAuth2Authentication, "company_no");
    }

    public static String getTeacherEmail(OAuth2Authentication oAuth2Authentication) {
        return getDetailsWithKey(oAuth2Authentication, "email");
    }

    private static String getDetailsWithKey(OAuth2Authentication oAuth2Authentication, String key) {
        Object details = oAuth2Authentication.getDetails();
        if (details instanceof OAuth2AuthenticationDetails) {
            OAuth2AuthenticationDetails oAuth2AuthenticationDetails = (OAuth2AuthenticationDetails) details;
            Map<String, Object> decodedDetails = (Map<String, Object>) oAuth2AuthenticationDetails.getDecodedDetails();
            return decodedDetails.get(key).toString();
        } else {
            throw new RuntimeException("Not found company_no");
        }
    }
}
