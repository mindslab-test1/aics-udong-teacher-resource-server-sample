package ai.mindslab.daekyo.teacher.core.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.Instant;

@Getter
@Setter
@ToString
public class Teacher implements Cloneable {
    // primary key
    private Long id;

    // unique (This will be used as the user login id)
    private Long companyNo;
    private String passwordHashed;

    private String birth;
    private String gender;
    private String name;
    private String imageUrl;
    private String tel;

    // unique
    private String email;

    private String schedule;
    private String request;

    // Reset Password Token
    private String resetPasswordToken;
    private Instant resetPasswordTokenExpiredAt;

    private String reading;
    private String coaching;

    private boolean locked;
    private boolean active;

    // ISO-8601
    private Instant createdAt;
    private Instant updatedAt;

    @Builder
    public Teacher(Long id, Long companyNo, String passwordHashed, String birth, String gender, String name, String imageUrl, String tel, String email, String schedule, String request, String resetPasswordToken, Instant resetPasswordTokenExpiredAt, String reading, String coaching, boolean locked, boolean active, Instant createdAt, Instant updatedAt) {
        this.id = id;
        this.companyNo = companyNo;
        this.passwordHashed = passwordHashed;
        this.birth = birth;
        this.gender = gender;
        this.name = name;
        this.imageUrl = imageUrl;
        this.tel = tel;
        this.email = email;
        this.schedule = schedule;
        this.request = request;
        this.resetPasswordToken = resetPasswordToken;
        this.resetPasswordTokenExpiredAt = resetPasswordTokenExpiredAt;
        this.reading = reading;
        this.coaching = coaching;
        this.locked = locked;
        this.active = active;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    @Override
    public Teacher clone() {
        try {
            return (Teacher) super.clone();
        } catch (CloneNotSupportedException e) {
            return Teacher.builder()
                    .companyNo(this.companyNo)
                    .passwordHashed(this.passwordHashed)
                    .email(this.email)
                    .name(this.name)
                    .locked(this.locked)
                    .active(this.active)
                    .resetPasswordToken(this.resetPasswordToken)
                    .resetPasswordTokenExpiredAt(this.resetPasswordTokenExpiredAt)
                    .build();
        }
    }

    public boolean checkToken(String requestToken) {
        return requestToken.equalsIgnoreCase(this.resetPasswordToken);
    }

    public boolean checkTokenExpiredAt() {
        return this.resetPasswordTokenExpiredAt.isAfter(Instant.now());
    }
}
