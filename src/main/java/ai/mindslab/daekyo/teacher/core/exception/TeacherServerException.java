package ai.mindslab.daekyo.teacher.core.exception;

public class TeacherServerException extends RuntimeException {
    public TeacherServerException(String message) {
        super(message);
    }

    public TeacherServerException(String message, Throwable cause) {
        super(message, cause);
    }
}
