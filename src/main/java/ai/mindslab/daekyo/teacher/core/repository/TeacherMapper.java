package ai.mindslab.daekyo.teacher.core.repository;

import ai.mindslab.daekyo.teacher.core.model.Teacher;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface TeacherMapper {
    Teacher findByCompanyNo(Long companyNo);
}
