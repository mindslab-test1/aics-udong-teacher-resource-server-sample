package ai.mindslab.daekyo.teacher.infra.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class JwtSignKeyConfig {
    @Value("${jwt.public-key}")
    private String jwtPublicKey;

    @Value("${jwt.sign-key:daekyo-teacher-mindslab1234}")
    private String jwtSignKey;
}
