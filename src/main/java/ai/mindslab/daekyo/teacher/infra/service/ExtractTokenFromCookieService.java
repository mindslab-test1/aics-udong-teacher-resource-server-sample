package ai.mindslab.daekyo.teacher.infra.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.authentication.TokenExtractor;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Optional;

@Component
public class ExtractTokenFromCookieService implements TokenExtractor {
    private static final Logger logger = LoggerFactory.getLogger(ExtractTokenFromCookieService.class);

    @Override
    public Authentication extract(HttpServletRequest httpServletRequest) {
        if (httpServletRequest.getCookies() == null || httpServletRequest.getCookies().length == 0) {
            return null;
        }

        Optional<Cookie> accessTokenCookieOptional = Arrays.stream(httpServletRequest.getCookies())
                                                        .filter(cookie -> cookie.getName().equalsIgnoreCase("access_token"))
                                                        .findFirst();

        // 회원가입일 경우 Access Token을 추출하면 안됨.
        // 추출할 경우, 인증이 됐다고 넘어가서 나중에 basic auth로 클라이언트 인증 하는 부분이 먹히지 않게 되는 버그가 있음.
        if (!httpServletRequest.getServletPath().equalsIgnoreCase("<회원가입 url>") && accessTokenCookieOptional.isPresent()) {
            Cookie accessTokenCookie = accessTokenCookieOptional.get();
            return new PreAuthenticatedAuthenticationToken(accessTokenCookie.getValue(), "");
        } else {
            return null;
        }
    }
}
