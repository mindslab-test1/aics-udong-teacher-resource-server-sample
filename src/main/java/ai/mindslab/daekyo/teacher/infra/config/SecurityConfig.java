package ai.mindslab.daekyo.teacher.infra.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@RequiredArgsConstructor
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * controller단 내에 들어오기 이전까지 http call진행중 필요한 검증절차 및 인증을 설정해주는 클래스입니다.
     * Override하지 않는다면, http.authorizeRequests().anyRequest().authenticated().and().formLogin().and().httpBasic();
     * 다음과 같이 default로 적용되어지기 때문에 반드시 재정의가 필요한 method입니다.
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .formLogin().disable()
                .headers().frameOptions().disable()
            .and()
                .authorizeRequests()
                    .antMatchers("/", "/css/**", "/img/**", "/js/**", "/font/**", "/favicon.ico", "/exception").permitAll()
                .anyRequest().authenticated()
            .and()
                .httpBasic()
            .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    /**
     * 어떤 요청을 무시할려는 설정이 필요하다면 해당 method를 override하면 됩니다.
     * 보통 인증을 도입하여 default 요청인 부분들을 무시처리할때나 SSR(like Thymeleaf, Mustache etc.)의 지원으로 정적 자원을 무시처리할때 설정합니다.
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                // Spring boot 기본적인 설정을 활용할 수 있음.
                // Reference Link: https://pupupee9.tistory.com/114
//                .requestMatchers(PathRequest.toStaticResources().atCommonLocations());
                .mvcMatchers("/favicon.ico")
                .antMatchers("/static/img/**")
                .antMatchers("/static/js/**")
                .antMatchers("/static/css/**")
                .antMatchers("/static/font/**");
    }
}
