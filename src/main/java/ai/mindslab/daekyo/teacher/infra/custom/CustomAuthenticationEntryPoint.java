package ai.mindslab.daekyo.teacher.infra.custom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {
    private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationEntryPoint.class);

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        logger.debug("custom authentication entry point");
//        // status를 401 에러로 지정
//        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
//        // json 리턴 및 한글깨짐 수정.
//        response.setContentType("application/json;charset=utf-8");
//        ObjectMapper objectMapper = new ObjectMapper();
//        final String regex = "([a-z])([A-Z]+)";
//        final String replacement = "$1_$2";
//        String code = authException.getClass().getSimpleName().replaceAll(regex, replacement).toLowerCase();
//        logger.debug(code);
//
//        TeacherApiResponseError teacherApiResponseError = TeacherApiResponseError.builder()
//                .statusCode(HttpStatus.UNAUTHORIZED.value())
//                .code(code.replaceAll("_exception", ""))
//                .message(authException.getMessage())
//                .build();
//
//        String jsonBody = objectMapper.writeValueAsString(teacherApiResponseError);
//
//        PrintWriter out = response.getWriter();
//        out.print(jsonBody);
//        out.flush();

        response.sendRedirect("/exception?preUrl=" + request.getRequestURI());
    }
}
