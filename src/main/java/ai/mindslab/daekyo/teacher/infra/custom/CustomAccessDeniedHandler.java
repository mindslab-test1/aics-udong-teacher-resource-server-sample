package ai.mindslab.daekyo.teacher.infra.custom;

import ai.mindslab.daekyo.teacher.core.exception.TeacherApiResponseError;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class CustomAccessDeniedHandler implements AccessDeniedHandler {
    private static final Logger logger = LoggerFactory.getLogger(CustomAccessDeniedHandler.class);

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            logger.warn("User: " + auth.getName()
                    + " attempted to access the protected URL: "
                    + request.getRequestURI());
        }
        // status를 403 에러로 지정
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        // json 리턴 및 한글깨짐 수정.
        response.setContentType("application/json;charset=utf-8");
        ObjectMapper objectMapper = new ObjectMapper();

        TeacherApiResponseError teacherApiResponseError = TeacherApiResponseError.builder()
                .statusCode(HttpStatus.UNAUTHORIZED.value())
                .code("access_denied")
                .message(accessDeniedException.getMessage())
                .build();

        String jsonBody = objectMapper.writeValueAsString(teacherApiResponseError);

        PrintWriter out = response.getWriter();
        out.print(jsonBody);
//        response.sendRedirect("/exception");
    }
}
