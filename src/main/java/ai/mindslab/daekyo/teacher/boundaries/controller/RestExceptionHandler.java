package ai.mindslab.daekyo.teacher.boundaries.controller;

import ai.mindslab.daekyo.teacher.core.exception.BaseException;
import ai.mindslab.daekyo.teacher.core.exception.TeacherApiResponseError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);

    @ExceptionHandler(BaseException.class)
    public ResponseEntity<Object> handle(BaseException exception, WebRequest request) {
        logger.warn("handleBaseException", exception);
        TeacherApiResponseError teacherApiResponseError = TeacherApiResponseError.builder()
                .statusCode(exception.getStatusCode().value())
                .code(exception.getCode())
                .message(exception.getMessage())
                .build();
        return handleExceptionInternal(exception, teacherApiResponseError, new HttpHeaders(), exception.getStatusCode(), request);
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<Object> handleMultipartFile(MaxUploadSizeExceededException exception, WebRequest request) {
        logger.error("handleMultipartFileException", exception);
        TeacherApiResponseError teacherApiResponseError = TeacherApiResponseError.builder()
                .statusCode(HttpStatus.PAYLOAD_TOO_LARGE.value())
                .code("file_size_limit_exceeded")
                .message(exception.getMessage())
                .build();
        return handleExceptionInternal(exception, teacherApiResponseError, new HttpHeaders(), HttpStatus.PAYLOAD_TOO_LARGE, request);
    }

    @Override
    protected ResponseEntity<Object> handleBindException(BindException exception, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.warn("handleBindException", exception);
        return handleExceptionInternal(exception, exception.getBindingResult(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
}
