package ai.mindslab.daekyo.teacher.boundaries.controller;

import ai.mindslab.daekyo.teacher.core.model.Teacher;
import ai.mindslab.daekyo.teacher.core.repository.TeacherMapper;
import ai.mindslab.daekyo.teacher.util.OAuth2AuthenticationUtils;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@RequiredArgsConstructor
@Controller
public class IndexController {
    private final TeacherMapper teacherMapper;

    private static final Logger logger = LoggerFactory.getLogger(IndexController.class);

    @GetMapping("/exception")
    public String exception() {
        return "exception";
    }

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/detail")
    @PreAuthorize("hasAnyAuthority('ROLE_TEACHER')")
    public String detail(Model model, OAuth2Authentication oAuth2Authentication) {
        String requestCompanyNo = OAuth2AuthenticationUtils.getTeacherCompanyNo(oAuth2Authentication);
        Teacher teacher = teacherMapper.findByCompanyNo(Long.parseLong(requestCompanyNo));
        model.addAttribute("teacher", teacher);
        return "detail";
    }
}
