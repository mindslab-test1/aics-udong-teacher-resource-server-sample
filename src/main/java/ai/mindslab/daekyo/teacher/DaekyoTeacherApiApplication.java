package ai.mindslab.daekyo.teacher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DaekyoTeacherApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(DaekyoTeacherApiApplication.class, args);
    }

}
