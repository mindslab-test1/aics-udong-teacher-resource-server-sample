import {auth} from "./util/constants.js";

const index = {
    init: function () {
        const _this = this;
        $('#login').on('click', function () {
            $('.login-warn').remove();
            _this.login();
        });

        $('#logout').on('click', function () {
            _this.logout();
        });
    },

    login: function () {

        const data = {
            grant_type: auth.grant_type.password,
            username: $('#teacher-id').val(),
            password: $('#password').val()
        };

        console.log(data);

        $.ajax({
            async: false,
            type: 'POST',
            url: auth.host + auth.uri,
            contentType: 'application/json; charset=utf-8',
            headers: {
                'Authorization': 'Basic ' + btoa(auth.authorization.clientId + ":" + auth.authorization.clientSecret)
            },
            dataType: 'json',
            data: JSON.stringify(data)
        }).done(function (data) {
            console.log(data);
            const res = data;
            alert('로그인 성공하셨습니다.');

            const decoded_access_token = jwt_decode(res.access_token);

            // set cookie access-token
            // develop => secure: false
            // production => secure: true (https)
            $.cookie('access_token', res.access_token, {
                expires: moment.unix(decoded_access_token.exp).toDate(),
                path: '/',
                secure: false
            });

            const decoded_refresh_token = jwt_decode(res.refresh_token);

            // set cookie refresh-token
            // develop => secure: false
            // production => secure: true (https)
            $.cookie('refresh_token', res.refresh_token, {
                expires: moment.unix(decoded_refresh_token.exp).toDate(),
                path: '/',
                secure: false
            });

            window.location.replace('/detail');
        }).fail(function (data) {
            const response = data.responseJSON;
            console.warn(response.error + ':' + response.error_description);
            // TODO: error handling
            $("#password").after("<p class='login-warn' style='color: red;'>UserId 또는 Password가 일치하지 않습니다. (" + response.error_description + ")</p>");
        });
    },

    logout: function () {
        console.log('logout');
        $.removeCookie('access_token');
        $.removeCookie('refresh_token');
        window.location.href = '/';
    },
};

index.init();
