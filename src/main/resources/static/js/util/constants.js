export const auth = {
    host: 'https://aics-test.maum.ai',
    uri: '/daekyo/oauth/token',
    authorization: {
        clientId: 'daekyo-teacher',
        clientSecret: 'daekyo-teacher-secret12345'
    },
    grant_type: {
        password: 'password',
        refreshToken: 'refresh_token'
    }
}
