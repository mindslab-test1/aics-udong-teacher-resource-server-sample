function checkJwtTokenInCookie() {
    // TODO: refactoring switch range condition..? please check
    if (isAllTokensPresent()) {
        console.log('exist access_token, refresh_token');
    }
    else if (isRefreshTokenPresent()) {
        console.log('not exist access_token');
        if (window.location.pathname === '/') {
            console.log('exist refresh_token');
            $.removeCookie('refresh_token');
            window.location.reload();
            return;
        }
        const data = {
            grant_type: 'refresh_token',
            refresh_token: $.cookie('refresh_token')
        };

        $.ajax({
            async: false,
            type: 'POST',
            url: 'http://localhost:8081/oauth/token',
            headers: {
                'Authorization': 'Basic ' + btoa('daekyo-teacher:daekyo-teacher-secret12345')
            },
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(data)
        }).done(function (data, textStatus, jqXHR) {
            const res = data;

            const decoded_access_token = jwt_decode(res.access_token);

            // set cookie access-token
            // develop => secure: false
            // production => secure: true (https)
            $.cookie('access_token', res.access_token, {
                expires: moment.unix(decoded_access_token.exp).toDate(),
                path: '/',
                secure: false
            });

            window.location.replace(getUrlParameter('preUrl'));
        }).fail(function (data) {
            const response = data.responseJSON;
            console.warn(response.error);
            alert(response.error_description);
            // TODO: error handling
        });
    }
    else if (isNotRefreshTokenPresent()) {
        console.log('exist access_token but not exist refresh_token');
        if (window.location.pathname !== '/') {
            alert('not authentication user. redirect login page');
            $.removeCookie('access_token');
            window.location.href = '/';
        }
    }
    else {
        console.log('required login');
        if (window.location.pathname !== '/') {
            alert('not authentication user. redirect login page');
            window.location.href = '/';
        }
    }
}

function isAllTokensPresent() {
    return ($.cookie('access_token') && $.cookie('refresh_token'));
}

function isRefreshTokenPresent() {
    return (!$.cookie('access_token') && $.cookie('refresh_token'));
}

function isNotRefreshTokenPresent() {
    return ($.cookie('access_token') && !$.cookie('refresh_token'));
}

function getUrlParameter(sParam) {
    let sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
}

checkJwtTokenInCookie();
